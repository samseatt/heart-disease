
#########################################################
# Prerequisites / preparation steps
#########################################################
# This file uses the Heart Disease UCI data set.

# The goal of this R script file is to load the data, statistically analyze it, clean it,
# plot or list it as necessary, split it into training and validation sets, save these data sets,
# develop various classificaiton models, train each model on the same trainng set of this data,
# run the predictions to evaluate the results using a handful of parameters, and make snippets
# of these code available for analysis (in the RMD file) for final presentation of the data,
# the methods, and the final results

# This script (model.R) is also available in my RStudio project checked in GitHub ()


# ??
# In my data partition code I also save the data in an RData (rda) object: edx.rda
# This way you don't have to load the data everytime you lose your edx R session/environment variable
# You can subsequently load it using the following code (I have commented it your since you don't have
# my RDA ojbect, however you can grab it from my github project).

library(tidyverse)

# ??

# First get the data from the Kaggle repository and save it as 'data/heart.csv'
# https://www.kaggle.com/ronitf/heart-disease-uci/downloads/heart-disease-uci.zip/1

# dl <- tempfile()
# download.file("https://www.kaggle.com/ronitf/heart-disease-uci/downloads/heart-disease-uci.zip/1", "data/heart-disease-uci.zip")


# https://www.kaggle.com/ronitf/heart-disease-uci/downloads/heart-disease-uci.zip

# Understand data distribution of each field (this helps with... )





